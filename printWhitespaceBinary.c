#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <string.h>

int main(int argc, char *argv[])
{
    if(argc <= 1)
    {
        printf("Missing paramter: path to binary\n");
    }

    FILE* f = fopen(argv[1], "r");

    fseek(f, 0, SEEK_END);
    unsigned int len = ftell(f);
    fseek(f, 0, SEEK_SET);

    char* bin = (char*)malloc(len);
    fread(bin, 1, len, f);

    fclose(f);

    char* whitespaceBin = (char*)malloc(len*8 + 1);
    for(int i = 0; i < len; ++i)
    {
        for(int bit = 0; bit < 8; ++bit)
        {
            int index = i*8 + bit;
            if((bin[i] & (0b1 << bit)) != 0)
                whitespaceBin[index] = ' ';
            else
                whitespaceBin[index] = '\t';
        }
    }
    whitespaceBin[len*8] = '\0';

    printf("char* data = \"%s\";\n", whitespaceBin);
    free(whitespaceBin);
    free(bin);
}
