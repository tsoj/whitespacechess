ifndef COMP
	COMP = cc
endif

ifndef BINARY_NAME
	BINARY_NAME = Nalwald-15
endif

all: prog

prog: prog.c
	$(COMP) prog.c -o prog

prog.c: printWhitespaceBinary $(BINARY_NAME) runWhiteSpaceBinary.c
	./printWhitespaceBinary $(BINARY_NAME) > prog.c
	cat runWhiteSpaceBinary.c >> prog.c

printWhitespaceBinary: printWhitespaceBinary.c
	$(COMP) printWhitespaceBinary.c -o printWhitespaceBinary

test: prog
	./prog

clean:
	rm ./prog ./prog.c ./printWhitespaceBinary
