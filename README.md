# WhitespaceChess

**Smallest chess program ever** *(if you don't count whitespaces)*

## Compile and Run

```
cc prog.c -o prog
./prog
```

## Create `prog.c` using own binary
```
make clean
make prog.c BINARY_NAME=your-binary
```
The resulting `prog.c` file will have the whitespaced source of your binary in it. You can then compile it like describe above.